/*Write down a program to store the details of students using the knowledge of loops, arrays and structures you gained so far. First name of the student,
subject and its marks for each student needs to be recorded. Data should be fetched through user input (do not hard code) and print the data back.
Program should record and display information of minimum 5 students.
Hint: use array to hold structures and use loops to access structures in the array*/

#include<stdio.h>

struct Student{
    char first_name[10];
    char subject[10];
    int mark;
};

int main(){
    int i;
 struct Student student[6];

    printf("Details of the students: \n");

for( i=0;i<6;i++){
     printf("\nstudent %d details \n",i+1);
    printf("Enter first name of the student: \n");
    scanf("%s",&student[i].first_name);
    printf("Enter a subject: \n");
    scanf("%s",&student[i].subject);
    printf("Enter the subject mark: \n");
    scanf("%d",&student[i].mark);
}
for( i=0;i<6;i++){
    printf("\nstudent %d details\n ",i+1);
   printf("\nFirst name:%s",student[i].first_name);
    printf("\nSubject:%d",student[i].subject);
     printf("\nMark: %d \n ",student[i].mark);
}
return 0;
}
